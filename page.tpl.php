<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=iso-8859-1" />
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
<title><?php print $head_title ?></title>
</head>



<body>

<div id="container">

<div id="header"></div>

<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'navcontainer', 'id' => 'navlist')) ?><?php } ?>


<div id="wrapper">
<?php if ($sidebar_left) { ?>
<div id="left">
  <ul class="list">
       <?php print $sidebar_left ?>
</ul>
</div>
<?php } ?>
  
  
<?php if ($sidebar_right) { ?>
<div id="right">
<?php print $sidebar_right ?>
</div>
<?php } ?> 
  
<div id="content">
          <?php if ($mission) { ?><div class="mission"><?php print $mission ?></div><?php } ?>
          <?php if ($breadcrumb) { ?><div class="breadcrumb"><?php print $breadcrumb ?></div><?php } ?>
          <?php if ($title) { ?><h2 class="pageTitle"><?php print $title ?></h2><?php } ?>
          <?php if ($tabs) { ?><div class="tabs"><?php print $tabs ?></div><?php } ?>
          <?php if ($help) { ?><div class="help"><?php print $help ?></div><?php } ?>
          <?php if ($messages) { ?><div class="messages"><?php print $messages ?></div><?php } ?>
	  <?php print $content_top; ?>
          <?php print $content; ?>
	  <?php print $content_bottom; ?>
	  <?php print $feed_icons; ?>
			<div style="clear: both"></div>
</div>


</div>


<div id="footer">
      <?php print $footer_message ?>
    <!-- It is greatly appreciated that you leave the below theme information here -->
<div id="info"><p>Design by <a href="www.csstemplateheaven.com">Dieter Schneider</a> 2006; Ported to <a href="http://drupal.org/project/purple_beauty">Drupal</a> by <a href="http://www.ronsnexus.com/">Ron Williams</a> for <a href="http://www.lithicmedia.com">Lithic Media</a></p></div>
</div>


</div>
<?php print $closure ?>
</body>
</html>
